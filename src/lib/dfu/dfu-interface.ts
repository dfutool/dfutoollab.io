import type {DFUDevice} from '$lib/dfu/dfu-device';
import type {DFUConfiguration} from '$lib/dfu/dfu-configuration';

export class DFUInterface {
    public corrupt: boolean;

    constructor(
        public readonly isTrigger: boolean,
        public readonly config: DFUConfiguration,
        public readonly interfaceId: number,
        public readonly settingId: number,
        public readonly settingName: string,
        public readonly device: DFUDevice,
    ) {
    }

    async init(): Promise<void> {
        if (!this.isTrigger) {
            await this.claim();
            this.corrupt = (await this.getStatus()).corrupt;
            await this.device.usb.releaseInterface(this.interfaceId);
        }
    }

    async claim(): Promise<void> {
        const usb = this.device.usb;

        await usb.open();
        await usb.selectConfiguration(this.config.configurationValue);
        await usb.claimInterface(this.interfaceId);
        await usb.selectAlternateInterface(this.interfaceId, this.settingId);
    }

    async detach(forceReset = false): Promise<void> {
        const usb = this.device.usb;

        await this.claim();
        await this.dfuOut(0x0, Math.min(this.config.dfuDetachTimeout, 1000));

        if (!this.config.dfuWillDetach || forceReset) {
            await usb.reset();
        }
    }

    private async dfuIn(request: number, value: number, length: number): Promise<DataView> {
        const result = await this.device.usb.controlTransferIn({
            requestType: 'class',
            recipient: 'interface',
            request,
            value,
            index: this.interfaceId
        }, length);

        if (result.status !== 'ok') {
            throw new Error(`dfuIn status: ${result.status}`);
        }

        if (result.data.byteLength !== length) {
            throw new Error('dfuIn length mismatch');
        }

        return result.data;
    }

    private async dfuOut(request: number, value: number, data?: BufferSource): Promise<void> {
        const usb = this.device.usb;

        console.log(`dfuOut req: ${request} | val: ${value} | data: ${data ? data.byteLength : null}`);
        const result = await usb.controlTransferOut({
            requestType: 'class',
            recipient: 'interface',
            request,
            value,
            index: this.interfaceId
        }, data);

        if (result.status !== 'ok') {
            throw new Error(`dfuIn status: ${result.status}`);
        }
    }

    public async getStatus(): Promise<{ corrupt: boolean, pollTimeout: number, state: number }> {
        const data = await this.dfuIn(0x3, 0, 6);

        const status = data.getUint8(0);
        const pollTimeout = data.getUint32(0, true) >> 8;
        const state = data.getUint8(4);

        console.log(`state: ${state} | status: ${status}`);
        if (state === 10) {
            // CLEARSTATE
            await this.dfuOut(0x4, 0);
            if (status === 0x0A) {
                return {corrupt: true, pollTimeout, state};
            }
            throw new Error(`DFU Error status: ${status}`);
        }
        return {corrupt: false, pollTimeout, state};
    }

    private async waitState(states: number[]): Promise<number> {
        // eslint-disable-next-line no-constant-condition
        while (true) {
            const status = await this.getStatus();
            if (states.includes(status.state)) {
                return status.state;
            } else if (status.state !== 4 && status.state !== 7) {
                throw new Error(`Unexpected DFU state: ${status.state}`);
            }
            await new Promise(r => setTimeout(r, status.pollTimeout));
        }
    }

    private async downloadBlock(blockNumber: number, block?: BufferSource): Promise<void> {
        await this.dfuOut(0x1, blockNumber, block);
        // Wait for dfuDNLOAD-IDLE (5) or dfuMANIFEST(7)
        await this.waitState([block ? 5 : 7]);


    }

    async download(buf: ArrayBuffer): Promise<void> {
        const usb = this.device.usb;
        await this.claim();

        const blockSize = Math.min(0x1000, this.config.dfuTransferSize);

        const blockCount = Math.ceil(buf.byteLength / blockSize);

        console.log(`blockSize: ${blockSize} | blockCount: ${blockCount}`);

        for (let i = 0; i < blockCount; i++) {
            await this.downloadBlock(i, buf.slice(i * blockSize, (i + 1) * blockSize));
        }
        await this.downloadBlock(blockCount);
        const state = await this.waitState([2, 8]);
        if (state === 8) {
            await usb.reset();
        } else {
            await usb.releaseInterface(this.interfaceId);
        }
    }
}
