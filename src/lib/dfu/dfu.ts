import {DFUDevice} from '$lib/dfu/dfu-device';

export async function pair(): Promise<DFUDevice> {
    const usbDevice = await navigator.usb.requestDevice({
        filters: [
            {classCode: 0xFE, subclassCode: 0x01, protocolCode: 0x01}, // DFU Trigger Device
            {classCode: 0xFE, subclassCode: 0x01, protocolCode: 0x02}, // DFU Bootloader Device
        ]
    });

    const device = new DFUDevice(usbDevice);
    await device.init();
    return device;
}

export async function getPaired(): Promise<DFUDevice[]> {
    const usbDevices = await navigator.usb.getDevices();

    const devices = usbDevices.map(usb => new DFUDevice(usb));
    for (const device of devices) {
        await device.init();
    }
    return devices;
}
