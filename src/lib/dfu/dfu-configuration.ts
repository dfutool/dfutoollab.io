import type {DFUDevice} from '$lib/dfu/dfu-device';

export class DFUConfiguration {
    dfuWillDetach: boolean;
    dfuDetachTimeout: number;
    dfuTransferSize: number;

    constructor(
        readonly configurationValue: number,
        readonly device: DFUDevice,
    ) {
    }
}
