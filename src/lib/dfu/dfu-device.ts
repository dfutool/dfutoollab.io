import {DFUInterface} from '$lib/dfu/dfu-interface';
import {DFUConfiguration} from '$lib/dfu/dfu-configuration';

export class DFUDevice {
    public readonly interfaces: DFUInterface[] = [];
    public readonly configurations: DFUConfiguration[] = [];
    public readonly name: string;
    public hidden = false;

    constructor(
        public readonly usb: USBDevice
    ) {
        this.name = usb.productName;
    }

    private static sliceBuffer(data: DataView, size: number): DataView[] {
        if (data.byteLength < size) {
            throw new Error('Data smaller than size');
        }

        return [
            new DataView(data.buffer, data.byteOffset, size),
            new DataView(data.buffer, data.byteOffset + size, data.byteLength - size)
        ];
    }

    private static sliceDescriptor(data: DataView): DataView[] {
        const size = data.getUint8(0);
        return DFUDevice.sliceBuffer(data, size);
    }

    private async getDescriptor(value: number, index: number): Promise<DataView> {
        return this.usb.controlTransferIn(
            {
                requestType: 'standard',
                recipient: 'device',
                request: 0x06,
                value,
                index,
            }, 0x1000)
            .then(result => {
                if (result.status !== 'ok') {
                    throw new Error(`Get Device Descriptor status: ${result.status}`);
                }
                return result.data;
            });
    }

    private async getStringDescriptor(index: number, lang: number): Promise<Uint16Array> {
        return new Uint16Array((await this.getDescriptor(0x0300 + index, lang)).buffer).slice(1);
    }

    private async getStringDescriptorAsString(index: number, lang: number): Promise<string> {
        const desc = await this.getStringDescriptor(index, lang);
        return String.fromCharCode(...desc);
    }

    async init(): Promise<void> {
        for (let i = 0; i < this.usb.configurations.length; i++) {
            await this.usb.open().then(() => this.getDescriptor(0x0200 + i, 0))
                .then(async (data) => {

                    // eslint-disable-next-line prefer-const
                    let [configDesc, rest] = DFUDevice.sliceDescriptor(data);

                    if (configDesc.byteLength < 9 || configDesc.getUint8(1) !== 2) {
                        throw new Error('Malformed Configuration Descriptor');
                    }

                    const config = new DFUConfiguration(configDesc.getUint8(5), this);
                    const lang = (await this.getStringDescriptor(0, 0))[0];

                    let dfuDescSeen = false;
                    while (rest.byteLength) {
                        let descriptor: DataView;
                        [descriptor, rest] = DFUDevice.sliceDescriptor(rest);

                        switch (descriptor.getUint8(1)) {
                            case 0x4: {
                                if (descriptor.byteLength < 9) {
                                    throw new Error('Malformed Descriptor');
                                }

                                const bProtocol = descriptor.getUint8(7);
                                if (descriptor.getUint8(5) === 0xFE &&
                                    descriptor.getUint8(6) === 0x01 &&
                                    (bProtocol === 0x01 || bProtocol === 0x02)
                                ) {
                                    const intf = new DFUInterface(
                                        bProtocol === 1,
                                        config,
                                        descriptor.getUint8(2),
                                        descriptor.getUint8(3),
                                        await this.getStringDescriptorAsString(descriptor.getUint8(8), lang),
                                        this
                                    );
                                    await intf.init();
                                    this.interfaces.push(intf);
                                }
                                break;
                            }
                            case 0x21:
                                if (descriptor.byteLength < 9) {
                                    throw new Error('Malformed Descriptor');
                                }
                                dfuDescSeen = true;
                                config.dfuWillDetach = !!((descriptor.getUint8(2) >> 3) & 1);
                                config.dfuDetachTimeout = descriptor.getUint16(3, true);
                                config.dfuTransferSize = descriptor.getUint16(5, true);
                        }
                    }

                    if (!dfuDescSeen) {
                        throw new Error('DFU Functional Descriptor missing!');
                    }
                })
                .catch(e => {
                    // TODO: better error handling
                    this.hidden = true;
                    console.error(e);
                });
        }
    }
}
